//
// Created by karol on 2021-05-16.
//

#ifndef INC_2LAB_PACKET_H
#define INC_2LAB_PACKET_H

#include <cstdint>
#include <string>

#define PACKET_COMMAND_REQUEST 1 //Requests for packet table.
#define PACKET_COMMAND_RESPONSE 2 //Response of packet table.
#define PACKET_COMMAND_TRACE_ON 3 //Obsolete
#define PACKET_COMMAND_TRACE_OFF 4 //Obsolete
#define PACKET_COMMAND_RESERVED 5 //Reserved by sun microsystems.
#define PACKET_COMMAND_MESSAGE 6 //Custom by us just sends packet and prints it.

namespace LAB03 {
    class Packet {
        uint8_t command;
        uint8_t version = 1;
        uint16_t zero1 = 0;
        uint16_t addressFamilyIdentifier;
        uint16_t zero2 = 0;
        std::string routerAddress;
        uint32_t zero3 = 0;
        uint32_t zero4 = 0;
        uint32_t metric;
        std::string payload;

    public:
        Packet(uint8_t command, std::string const& routerAddress, std::string const& payload);
        ~Packet();

        uint8_t getCommand() const;
        uint8_t getVersion() const;
        uint16_t getAddressFamilyIdentifier() const;
        const std::string &getRouterAddress() const;
        uint32_t getMetric() const;
        const std::string &getPayload() const;
    };
}


#endif //INC_2LAB_PACKET_H
