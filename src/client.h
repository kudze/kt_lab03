//
// Created by karol on 2021-04-18.
//

#ifndef INC_2LAB_CLIENT_H
#define INC_2LAB_CLIENT_H

#include "console.h"
#include "commands.h"
#include "network.h"

namespace LAB03 {
    class Client {
    private:
        bool shouldRun = true;
        Console* console;
        L5RP::CommandManager* commandManager;
        Network* network;

        void greetClient();
        void registerClientCommands();

    public:
        explicit Client(Console* console);
        ~Client();

        void run();
        void stop();

        L5RP::CommandManager* getCommandManager() const;
        Console* getConsole() const;
        Network* getNetwork() const;
    };
}


#endif //INC_2LAB_CLIENT_H
