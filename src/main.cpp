//
// Created by karol on 2021-04-17.
//

#include <iostream>
#include <stdexcept>

#include "client.h"

using namespace std;

int main(int argc, char *argv[]) {
    auto client = new LAB03::Client(
            new Console()
    );

    client->run();

    delete client;

    return 0;
}