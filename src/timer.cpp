//
// Created by karol on 2021-05-16.
//

#include <iostream>
#include "timer.h"
#include "network.h"

using namespace LAB03;

Timer::Timer(Network *network) {
    this->network = network;
}

Timer::~Timer() = default;

void Timer::makeAllRoutersUpdate() {

    for(auto const& pair : this->getNetwork()->getRoutersMap())
    {
        std::cout << std::endl << "Marking " << pair.first << " router to send table to neighbours:" << std::endl;
        pair.second->sendTableToNeighboursNextTick();

        std::cout << "Sleeping for neighbourgs tables to get updated" << std::endl << std::endl << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds (2));
    }

}

Network *Timer::getNetwork() const {
    return network;
}
