//
// Created by karol on 2021-05-15.
//

#ifndef INC_2LAB_NETWORK_H
#define INC_2LAB_NETWORK_H

#include <map>

#include "router.h"
#include "timer.h"

namespace LAB03 {
    class Network {
        std::map<std::string, Router*> routers;
        Timer* timer;

    public:
        Network();
        ~Network();

        Router* addRouter(std::string const& title);
        void removeRouter(std::string const& title);
        void removeRouter(Router* router);
        std::map<std::string, Router*> const& getRoutersMap() const;
        Router* getRouter(std::string const& title) const;

        void addWire(Router* routerA, Router* routerB, float cost);
        void removeWire(Router* routerA, Router* routerB);

        Timer *getTimer() const;
    };
}


#endif //INC_2LAB_NETWORK_H
