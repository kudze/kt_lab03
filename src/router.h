//
// Created by karol on 2021-05-15.
//

#ifndef INC_2LAB_ROUTER_H
#define INC_2LAB_ROUTER_H

#include <string>
#include <thread>
#include <atomic>
#include <map>
#include <mutex>

#include "wire.h"

namespace LAB03 {
    class Network;

    class TableRow {
        std::string next;
        float cost;

    public:
        TableRow(std::string const& next, float cost);
        ~TableRow() = default;

        const std::string &getNext() const;

        float getCost() const;
    };

    class Router {
        static std::mutex coutMutex;
        std::string title;
        std::atomic<bool> shouldRun;
        std::atomic<bool> shouldSendTableNextTick;
        std::thread thread;
        Network* network;

        std::mutex packetsMutex;
        std::vector<Packet> packetsToSend;

        std::mutex wiresMutex;
        std::map<std::string, Wire*> wires;

        std::mutex tableMutex;
        std::map<std::string, TableRow> table; //key is DST.

        void threadFunc();
        void sendPacket(Packet const& packet, bool lockWires = true);
        void sendTableToNeighbours(bool lockWires = true);

    public:
        Router(std::string const& title, Network* network);
        ~Router();

        void addWire(Wire* wire);
        void removeWire(Wire* wire);

        std::string const& getTitle() const;
        Network* getNetwork() const;
        std::map<std::string, Wire *> const& getWires() const;
        bool hasWireFor(Router* router) const;
        Wire* getWireFor(Router* router) const;

        void lockTable();
        void unlockTable();
        std::map<std::string, TableRow> const& getTable() const; //Once getting should always lockTable before and unlockTable after.

        void sendTableToNeighboursNextTick();

        void addPacketToSend(Packet const& packet);
    };
}


#endif //INC_2LAB_ROUTER_H
