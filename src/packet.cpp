//
// Created by karol on 2021-05-16.
//

#include "packet.h"

using namespace LAB03;

Packet::Packet(uint8_t command, const std::string &routerAddress, const std::string &payload) {
    this->command = command;
    this->version = 1;
    this->routerAddress = routerAddress;
    this->metric = payload.length();
    this->payload = payload;
}

Packet::~Packet() = default;

uint8_t Packet::getCommand() const {
    return command;
}

uint8_t Packet::getVersion() const {
    return version;
}

uint16_t Packet::getAddressFamilyIdentifier() const {
    return addressFamilyIdentifier;
}

const std::string &Packet::getRouterAddress() const {
    return routerAddress;
}

uint32_t Packet::getMetric() const {
    return metric;
}

const std::string &Packet::getPayload() const {
    return payload;
}
