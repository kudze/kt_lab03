//
// Created by karol on 2021-04-18.
//

#include "client.h"
#include "variadicTable.h"

#include <stdexcept>
#include <iostream>
#include <thread>
#include <chrono>

using namespace LAB03;
using namespace std;

Client::Client(Console *console) {
    this->console = console;
    this->network = new Network();
    this->commandManager = new L5RP::CommandManager(this);

    this->registerClientCommands();
    this->greetClient();
}

Client::~Client() {
    this->shouldRun = false;

    delete this->network;
    delete this->commandManager;
    delete this->console;
}

void Client::greetClient() {
    std::cout << "Welcome to Karolis Kraujelis network simulator" << std::endl;
    std::cout << "Type help in order to see all available commands" << std::endl;
    std::cout << "Have fun! :)" << std::endl << std::endl;
}

void Client::registerClientCommands() {
    using namespace L5RP;

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                this->getNetwork()->addRouter(args[0]._string);
                std::cout << args[0]._string << " router was created succesfully!" << std::endl;
            },
            {
                    "add_router"
            },
            {
                    new Argument(
                            new StringArgument(),
                            "title",
                            "title of a router"
                    )
            },
            {

            },
            "Creates a router"
    );

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                this->getNetwork()->removeRouter(args[0]._string);
                std::cout << args[0]._string << " router was removed succesfully!" << std::endl;
            },
            {
                    "remove_router"
            },
            {
                    new Argument(
                            new StringArgument(),
                            "title",
                            "title of a router"
                    )
            },
            {

            },
            "Removes a router"
    );

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                if (this->getNetwork()->getRoutersMap().empty()) {
                    std::cout << "Network doesn't contain any routers." << std::endl;
                    return;
                }

                std::cout << "Routers in the network:" << std::endl;
                size_t i = 0;
                for (auto const &pair : this->getNetwork()->getRoutersMap()) {
                    std::cout << ++i << ": " << pair.first << std::endl;
                }
            },
            {
                    "routers"
            },
            {

            },
            {

            },
            "Lists all routers"
    );

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                auto router = args[0]._union._router;

                std::cout << "Title: " << router->getTitle() << std::endl;

                if (router->getWires().empty()) {
                    std::cout << "No wires connected to this router!" << std::endl;
                    return;
                }

                std::cout << "Wires connected to the router: " << std::endl;
                for (auto const &pair : router->getWires()) {
                    std::cout << "\t* " << pair.first << ": (Cost: " << pair.second->getCost() << ")" << std::endl;
                }

                std::cout << "Router's table:" << std::endl;
                VariadicTable<std::string, std::string, float> table(
                        {"Destination", "Next", "Cost"}
                );

                router->lockTable();
                for (auto const &pair : router->getTable()) {
                    table.addRow(pair.first, pair.second.getNext(), pair.second.getCost());
                }
                router->unlockTable();

                table.print(std::cout);
            },
            {
                    "router"
            },
            {
                    new Argument(
                            new RouterArgument(this->network),
                            "router",
                            "name of a router"
                    )
            },
            {

            },
            "Prints detailed info about router"
    );

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                auto routerA = args[0]._union._router;
                auto routerB = args[1]._union._router;
                auto cost = args[2]._union._float;

                this->getNetwork()->addWire(routerA, routerB, cost);
                std::cout << "Wire has been successfully created!" << std::endl;
            },
            {
                    "add_wire"
            },
            {
                    new Argument(
                            new RouterArgument(this->network),
                            "routerA",
                            "name of a router"
                    ),
                    new Argument(
                            new RouterArgument(this->network),
                            "routerB",
                            "name of a router"
                    ),
                    new Argument(
                            new FloatArgument(),
                            "cost",
                            "cost of a wire"
                    )
            },
            {

            },
            "Adds a wire to the network"
    );

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                auto routerA = args[0]._union._router;
                auto routerB = args[1]._union._router;

                this->getNetwork()->removeWire(routerA, routerB);
                std::cout << "Wire has been successfully removed!" << std::endl;
            },
            {
                    "remove_wire"
            },
            {
                    new Argument(
                            new RouterArgument(this->network),
                            "routerA",
                            "name of a router"
                    ),
                    new Argument(
                            new RouterArgument(this->network),
                            "routerB",
                            "name of a router"
                    )
            },
            {

            },
            "Removes a wire from the network"
    );

    new Command(
            this->getCommandManager(),
            [this](std::vector<ArgumentValue> const &args) {
                this->getNetwork()->getTimer()->makeAllRoutersUpdate();
            },
            {
                    "update_tables"
            },
            {

            },
            {

            },
            "Makes all router tables to update."
    );

    new Command(
            this->getCommandManager(),
            [](std::vector<ArgumentValue> const &args) {
                Router* source = args[0]._union._router;
                Router* dest = args[1]._union._router;
                std::string payload = args[2]._string;

                if(source == dest)
                    throw std::runtime_error("source and dest are the same!");

                auto packet = Packet(
                        PACKET_COMMAND_MESSAGE,
                        dest->getTitle(),
                        payload
                        );

                source->addPacketToSend(packet);
                std::cout << "Packet has been sent!" << std::endl;
            },
            {
                    "send_packet"
            },
            {
                    new Argument(
                            new RouterArgument(this->network),
                            "source",
                            "name of a router"
                    ),
                    new Argument(
                            new RouterArgument(this->network),
                            "destination",
                            "name of a router"
                    ),
                    new Argument(
                            new StringDumpArgument(),
                            "payload",
                            "payload (can be seperated by space)"
                    )
            },
            {

            },
            "Makes all router tables to update."
    );
}

void Client::run() {
    while (this->shouldRun) {
        auto inputs = this->console->getInputsIfAvailable();
        if (!inputs.empty()) {

            for (string const &input : inputs) {
                if (input.empty())
                    continue;

                this->commandManager->tryToRunCommand(input);
            }

        }

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void Client::stop() {
    this->shouldRun = false;
}

L5RP::CommandManager *Client::getCommandManager() const {
    return this->commandManager;
}

Console *Client::getConsole() const {
    return this->console;
}

Network *Client::getNetwork() const {
    return this->network;
}