//
// Created by karol on 2021-05-15.
//

#include "network.h"

#include <stdexcept>

using namespace LAB03;

Network::Network() {
    this->timer = new Timer(this);
}

Network::~Network() {
    for(auto const& pair : this->routers)
        delete pair.second;

    this->routers.clear();
    delete this->timer;
}

Router * Network::addRouter(const std::string &title) {
    if(this->routers.find(title) != this->routers.end())
        throw std::runtime_error("Network already contains router by the title of: " + title);

    auto result = new Router(title, this);
    this->routers[title] = result;
    return result;
}

void Network::removeRouter(Router *router) {
    this->removeRouter(router->getTitle());
}

void Network::removeRouter(const std::string &title) {
    auto it = this->routers.find(title);

    if(it == this->routers.end())
        throw std::runtime_error("Network doesn't contain router by the title of: " + title);

    delete it->second;
    this->routers.erase(it);
}

const std::map<std::string, Router *> & Network::getRoutersMap() const {
    return this->routers;
}

Router * Network::getRouter(const std::string &title) const {
    auto it = this->routers.find(title);

    if(it == this->routers.end())
        throw std::runtime_error("Network::getRouter no router found for given title");

    return it->second;
}

void Network::addWire(Router *routerA, Router *routerB, float cost) {
    if(routerA == routerB)
        throw std::runtime_error("A wire cannot end and start at the same router!");

    if(routerA->hasWireFor(routerB) || routerB->hasWireFor(routerA))
        throw std::runtime_error("This wire already exists!");

    Wire* wire = new Wire(routerA, routerB, cost);
    routerA->addWire(wire);
    routerB->addWire(wire);
}

void Network::removeWire(Router *routerA, Router *routerB) {
    if(!routerA->hasWireFor(routerB) || !routerB->hasWireFor(routerA))
        throw std::runtime_error("This wire doesnt exist!");

    auto wire = routerA->getWireFor(routerB);
    routerA->removeWire(wire);
    routerB->removeWire(wire);
    delete wire;
}

Timer *Network::getTimer() const {
    return timer;
}

