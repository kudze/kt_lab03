//
// Created by karol on 2021-04-18.
//

#ifndef INC_2LAB_CONSOLE_H
#define INC_2LAB_CONSOLE_H

#include <thread>
#include <vector>
#include <string>
#include <mutex>

class Console {
private:
    std::thread thread;
    std::vector<std::string> inputs;
    std::mutex inputMutex;

    void readConsoleFunc();

public:
    Console();
    ~Console();

    std::vector<std::string> getInputsIfAvailable();
};

#endif //INC_2LAB_CONSOLE_H
