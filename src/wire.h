//
// Created by karol on 2021-05-15.
//

#ifndef INC_2LAB_WIRE_H
#define INC_2LAB_WIRE_H

#include <vector>
#include <mutex>

#include "packet.h"

namespace LAB03 {
    class Router;
    class Wire {
        Router* routerA;
        Router* routerB;
        float cost;

        std::vector<Packet> packetsAtoB;
        std::vector<Packet> packetsBtoA;
        std::mutex packetsAtoBmutex;
        std::mutex packetsBtoAmutex;

    public:
        Wire(Router* routerA, Router* routerB, float cost);
        ~Wire();

        void addPacket(Router* sourceRouter, Packet const& packet);
        std::vector<Packet> getSentPackets(Router* destRouter);

        Router* getDestinationRouter(Router* sourceRouter) const;
        Router* getRouterA() const;
        Router* getRouterB() const;
        float getCost() const;
    };
}


#endif //INC_2LAB_WIRE_H
