//
// Created by karol on 2021-05-15.
//

#include "router.h"
#include "network.h"
#include "variadicTable.h"

#include <sstream>
#include <iostream>

using namespace LAB03;

#include <string>
#include <vector>
#include <sstream>
#include <utility>

std::mutex Router::coutMutex;

std::vector<std::string> explode(std::string const &s, char delim) {
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim);) {
        result.push_back(std::move(token));
    }

    return result;
}

Router::Router(const std::string &title, Network *network) {
    this->title = title;
    this->network = network;
    this->shouldRun = true;
    this->shouldSendTableNextTick = false;

    this->thread = std::thread(&Router::threadFunc, this);
}

Router::~Router() {
    this->shouldRun = false;
    this->thread.join();

    this->wiresMutex.lock();
    for (auto const &pair : this->wires) {
        auto router = this->getNetwork()->getRouter(pair.first);

        if (router != nullptr)
            router->removeWire(pair.second);
    }
    this->wiresMutex.unlock();
}

void Router::sendTableToNeighbours(bool lockWires) {
    std::stringstream ss;

    this->tableMutex.lock();
    for (auto const &pair : this->table) {
        ss << pair.first << "," << pair.second.getNext() << "," << pair.second.getCost() << '\n';
    }
    this->tableMutex.unlock();

    Packet packet = Packet(PACKET_COMMAND_RESPONSE, "*", ss.str());

    if(lockWires) this->wiresMutex.lock();
    for (auto const &pair : this->wires) {
        pair.second->addPacket(this, packet);
    }
    if(lockWires) this->wiresMutex.unlock();
}

void Router::sendPacket(const Packet &packet, bool lockWires) {

    this->tableMutex.lock();

    auto it = this->table.find(packet.getRouterAddress());
    if(it == this->table.end())
        std::cout << "ERROR: Can't sent a packet to " << packet.getRouterAddress() << " from " << this->getTitle() << " (Address is not in table)" << std::endl;

    else {
        if(lockWires) this->wiresMutex.lock();

        auto it2 = this->wires.find(it->second.getNext());
        if(it2 == this->wires.end())
            std::cout << "ERROR: Can't sent a packet to " << packet.getRouterAddress() << " from " << this->getTitle() << " (Next is not in wires (" << it->second.getNext() << "))" << std::endl;

        else {
            it2->second->addPacket(this, packet);
        }

        if(lockWires) this->wiresMutex.unlock();
    }

    this->tableMutex.unlock();

}

void Router::threadFunc() {
    while (this->shouldRun) {
        if (this->shouldSendTableNextTick == true) {
            this->sendTableToNeighbours();
            this->shouldSendTableNextTick = false;

            Router::coutMutex.lock();
            std::cout << "Router " << this->getTitle() << " sent its table to neighbours." << std::endl;
            Router::coutMutex.unlock();
        }

        this->packetsMutex.lock();
        for(auto const& packet : this->packetsToSend)
        {
            Router::coutMutex.lock();
            this->sendPacket(packet);
            std::cout << "Router " << this->getTitle() << " sent a message packet!" << std::endl;
            Router::coutMutex.unlock();
        }
        this->packetsToSend.clear();
        this->packetsMutex.unlock();

        this->wiresMutex.lock();
        for (auto const &pair : this->wires) {
            for (auto const &packet : pair.second->getSentPackets(this)) {
                Router::coutMutex.lock();
                std::cout << "Router " << this->getTitle() << " received a packet from " << pair.first << std::endl;

                if (packet.getCommand() == PACKET_COMMAND_RESPONSE) {
                    std::cout << "It's an table packet!" << std::endl;

                    std::map<std::string, TableRow> sentTable;
                    auto rows = explode(packet.getPayload(), '\n');
                    for (std::string const &row : rows) {
                        if (row.empty())
                            continue;

                        auto cells = explode(row, ',');
                        sentTable.insert(
                                std::pair<std::string, TableRow>(
                                        cells[0],
                                        TableRow(
                                                cells[1],
                                                std::stof(cells[2])
                                        )
                                )
                        );
                    }

                    {
                        std::cout << std::endl << "Recieved table: " << std::endl;
                        VariadicTable<std::string, std::string, float> _sentTable(
                                {"Destination", "Next", "Cost"}
                        );

                        for (auto const &_pair : sentTable) {
                            _sentTable.addRow(_pair.first, _pair.second.getNext(), _pair.second.getCost());
                        }

                        _sentTable.print(std::cout);
                    }

                    {
                        std::cout << std::endl << "Current table: " << std::endl;
                        VariadicTable<std::string, std::string, float> _currentTable(
                                {"Destination", "Next", "Cost"}
                        );

                        this->lockTable();
                        for (auto const &_pair : this->getTable()) {
                            _currentTable.addRow(_pair.first, _pair.second.getNext(), _pair.second.getCost());
                        }
                        this->unlockTable();

                        _currentTable.print(std::cout);
                    }

                    bool changed = false;
                    this->lockTable();
                    //Sync.
                    //Lets remove those one that weren't sent, cuz we only send full tables.
                    std::vector<std::string> toRemove;
                    for (auto const &_pair : this->table) {
                        if (_pair.second.getNext() != pair.first || (_pair.second.getNext() == _pair.first))
                            continue;
                        //Filters out next != wire end.

                        if (sentTable.find(_pair.first) == sentTable.end()) {
                            toRemove.push_back(_pair.first);
                        }
                    }

                    for (std::string const &routerName : toRemove) {
                        changed = true;
                        this->table.erase(
                                this->table.find(routerName)
                        );
                    }

                    for (auto const &_pair : sentTable) {
                        if (_pair.first == this->getTitle()) //If destination == current router we ignore this.
                            continue;

                        std::string next = pair.first; //next for inserted if we choose to use this will be the wire where it leads to.
                        float cost = pair.second->getCost() + _pair.second.getCost();
                        auto it = this->table.find(_pair.first);

                        if (it == this->table.end()) //If we dont have entry for this destination we add it.
                        {
                            if (cost <= 15) {
                                changed = true;
                                this->table.insert(
                                        std::pair<std::string, TableRow>(
                                                _pair.first, //DST is same as in sent.
                                                TableRow(
                                                        next,
                                                        cost
                                                )
                                        )
                                );
                            }
                        } else {
                            if (cost < it->second.getCost() || (it->second.getNext() == next && it->second.getCost() != cost)) {
                                changed = true;
                                this->table.erase(it);
                                if (cost <= 15)
                                    this->table.insert(
                                            std::pair<std::string, TableRow>(
                                                    _pair.first, //DST is same as in sent.
                                                    TableRow(
                                                            next,
                                                            cost
                                                    )
                                            )
                                    );
                            }
                        }
                    }
                    this->unlockTable();

                    {
                        std::cout << std::endl << "Updated table: " << std::endl;
                        VariadicTable<std::string, std::string, float> _currentTable(
                                {"Destination", "Next", "Cost"}
                        );

                        this->lockTable();
                        for (auto const &_pair : this->getTable()) {
                            _currentTable.addRow(_pair.first, _pair.second.getNext(), _pair.second.getCost());
                        }
                        this->unlockTable();

                        _currentTable.print(std::cout);
                    }

                    if(changed)
                    {
                        std::cout << "Routing table did change so this router is sending updated table to neighbourgs!" << std::endl;
                        this->sendTableToNeighbours(false);
                    }
                }
                else if(packet.getCommand() == PACKET_COMMAND_MESSAGE) {
                    std::cout << "Its an message packet!" << std::endl;

                    if(packet.getRouterAddress() == this->getTitle())
                    {
                        std::cout << "This packet has been sent for this router!" << std::endl;
                        std::cout << "Payload: " << packet.getPayload() << std::endl;
                    }

                    else {
                        this->sendPacket(packet, false);
                        std::cout << "Sent message further!" << std::endl;
                    }
                }

                Router::coutMutex.unlock();
            }
        }
        this->wiresMutex.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

const std::string &Router::getTitle() const {
    return this->title;
}

Network *Router::getNetwork() const {
    return this->network;
}

void Router::addWire(Wire *wire) {
    Router *destRouter = nullptr;

    try {
        destRouter = wire->getDestinationRouter(this);
    } catch (std::runtime_error const &e) {
        throw std::runtime_error("Router::addWire argument wire was passed that doesnt contain current router.");
    }

    auto it = this->wires.find(destRouter->getTitle());
    if (it != this->wires.end())
        throw std::runtime_error("Router::addWire argument wire was passed that wire is already added for this router");

    this->wiresMutex.lock();
    this->wires[destRouter->getTitle()] = wire;
    this->wiresMutex.unlock();

    this->tableMutex.lock();
    this->table.insert(std::pair<std::string, TableRow>(destRouter->getTitle(),
                                                        TableRow(destRouter->getTitle(), wire->getCost())));
    this->tableMutex.unlock();
}

void Router::removeWire(Wire *wire) {
    Router *destRouter = nullptr;

    try {
        destRouter = wire->getDestinationRouter(this);
    } catch (std::runtime_error const &e) {
        throw std::runtime_error("Router::removeWire argument wire was passed that doesnt contain current router.");
    }

    auto it = this->wires.find(destRouter->getTitle());
    if (it == this->wires.end())
        throw std::runtime_error("Router::removeWire argument wire was passed that isnt in current wires set.");

    this->wiresMutex.lock();
    this->wires.erase(it);
    this->wiresMutex.unlock();

    this->tableMutex.lock();
    auto it2 = this->table.find(destRouter->getTitle());
    if (it2 != this->table.end())
        this->table.erase(it2);
    this->tableMutex.unlock();
}

const std::map<std::string, Wire *> &Router::getWires() const {
    return wires;
}

bool Router::hasWireFor(Router *router) const {
    return this->wires.find(router->getTitle()) != this->wires.end();
}

Wire *Router::getWireFor(Router *router) const {
    auto it = this->wires.find(router->getTitle());

    if (it == this->wires.end())
        throw std::runtime_error("Router::getWireFor no wire exists for given router!");

    return it->second;
}

const std::map<std::string, TableRow> &Router::getTable() const {
    return table;
}

void Router::sendTableToNeighboursNextTick() {
    this->shouldSendTableNextTick = true;
}

void Router::addPacketToSend(const Packet &packet) {
    this->packetsMutex.lock();
    this->packetsToSend.push_back(packet);
    this->packetsMutex.unlock();
}

void Router::lockTable() {
    this->tableMutex.lock();
}

void Router::unlockTable() {
    this->tableMutex.unlock();
}

TableRow::TableRow(const std::string &next, float cost) {
    this->next = next;
    this->cost = cost;
}

const std::string &TableRow::getNext() const {
    return next;
}

float TableRow::getCost() const {
    return cost;
}
