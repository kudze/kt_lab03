//
// Created by karol on 2021-05-15.
//

#include "wire.h"

#include <stdexcept>

using namespace LAB03;

Wire::Wire(Router *routerA, Router *routerB, float cost) {
    this->routerA = routerA;
    this->routerB = routerB;
    this->cost = cost;
}

Wire::~Wire() {

}

Router *Wire::getRouterA() const {
    return routerA;
}

Router *Wire::getRouterB() const {
    return routerB;
}

float Wire::getCost() const {
    return cost;
}

Router *Wire::getDestinationRouter(Router *sourceRouter) const {
    if (this->routerA == sourceRouter)
        return this->routerB;

    if (this->routerB == sourceRouter)
        return this->routerA;

    throw std::runtime_error("Wire::getDestinationRouter argument sourceRouter isn't either router A nor router B");
}

void Wire::addPacket(Router *sourceRouter, Packet const& packet) {
    if (this->routerA == sourceRouter) {
        this->packetsAtoBmutex.lock();
        this->packetsAtoB.push_back(packet);
        this->packetsAtoBmutex.unlock();
        return;
    }

    if (this->routerB == sourceRouter) {
        this->packetsBtoAmutex.lock();
        this->packetsBtoA.push_back(packet);
        this->packetsBtoAmutex.unlock();
        return;
    }

    throw std::runtime_error("Wire::addPacket argument sourceRouter isn't either router A nor router B");
}

std::vector<Packet> Wire::getSentPackets(Router *destRouter) {
    if (this->routerA == destRouter) {
        this->packetsBtoAmutex.lock();
        auto copy = std::vector<Packet>(this->packetsBtoA);
        this->packetsBtoA.clear();
        this->packetsBtoAmutex.unlock();
        return copy;
    }

    if (this->routerB == destRouter) {
        this->packetsAtoBmutex.lock();
        auto copy = std::vector<Packet>(this->packetsAtoB);
        this->packetsAtoB.clear();
        this->packetsAtoBmutex.unlock();
        return copy;
    }

    throw std::runtime_error("Wire::getSentPackets argument destRouter isn't either router A nor router B");
}