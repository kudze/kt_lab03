//
// Created by karol on 2021-05-16.
//

#ifndef INC_2LAB_TIMER_H
#define INC_2LAB_TIMER_H

namespace LAB03 {
    class Network;
    class Timer {
        Network* network;

    public:
        explicit Timer(Network* network);
        ~Timer();

        void makeAllRoutersUpdate();

        Network *getNetwork() const;
    };
}


#endif //INC_2LAB_TIMER_H
