//
// Created by karol on 2021-04-18.
//

#include <iostream>
#include "console.h"

Console::Console() {
    this->thread = std::thread(&Console::readConsoleFunc, this);
}

Console::~Console() {
    this->thread.join();
}

void Console::readConsoleFunc() {
    while(true)
    {
        std::string input;
        std::getline(std::cin, input);

        this->inputMutex.lock();
        this->inputs.push_back(input);
        this->inputMutex.unlock();

        if(input == "exit")
            break;
    }
}

std::vector<std::string> Console::getInputsIfAvailable() {

    if(!this->inputMutex.try_lock())
        return std::vector<std::string>();

    auto result = std::vector<std::string>(this->inputs);
    this->inputs.clear();
    this->inputMutex.unlock();

    return result;
}

